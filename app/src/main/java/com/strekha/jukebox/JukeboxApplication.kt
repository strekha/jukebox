package com.strekha.jukebox

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate

class JukeboxApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        _instance = this
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
    }

    companion object {

        private var _instance: JukeboxApplication? = null
        val instance: JukeboxApplication
            get() = requireNotNull(_instance)
    }
}