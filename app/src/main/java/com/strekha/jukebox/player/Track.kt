package com.strekha.jukebox.player

import java.io.File
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

sealed interface AudioFile {

    data class Asset(val path: String) : AudioFile

    data class Path(val path: File) : AudioFile
}

@JvmInline
value class Volume(val value: Float) {

    init {
        require(value in MinValue..MaxValue)
    }

    companion object {

        const val MinValue = 0.0f
        const val MaxValue = 1.0f

        val Max = Volume(MaxValue)
    }
}

/**
 * Describes a delay between audio repeats
 */
@JvmInline
value class Frequency(val delay: Duration) {

    init {
        require(delay in MinValue..MaxValue)
    }

    companion object {

        val MinValue = 0.milliseconds
        val MaxValue = 5.seconds

        val Default = Frequency(MinValue)
    }
}

