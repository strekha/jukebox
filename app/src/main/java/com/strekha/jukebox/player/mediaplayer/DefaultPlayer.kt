package com.strekha.jukebox.player.mediaplayer

import android.content.Context
import android.media.MediaPlayer
import android.media.MediaPlayer.OnCompletionListener
import android.net.Uri
import com.strekha.jukebox.player.AudioFile
import com.strekha.jukebox.player.Frequency
import com.strekha.jukebox.player.Player
import com.strekha.jukebox.player.RepeatCount
import com.strekha.jukebox.player.Stream
import com.strekha.jukebox.player.Volume
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.currentCoroutineContext
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import java.util.concurrent.atomic.AtomicInteger
import kotlin.coroutines.resume
import kotlin.time.Duration.Companion.milliseconds

/**
 * [Player] instance which uses android's [android.media.MediaPlayer] as an engine
 */
class DefaultPlayer(
    private val context: Context,
) : Player {

    private val count = AtomicInteger(0)

    private val playlist = HashMap<Stream, StreamPlayer>()

    override fun observeProgress(stream: Stream): Flow<Float> {
        return playlist[stream]?.progress ?: flowOf(0f)
    }

    override fun isPlaying(stream: Stream): Boolean {
        return playlist[stream]?.isPlaying ?: false
    }

    override fun resume(stream: Stream) {
        playlist[stream]?.resume(byUser = true)
    }

    override fun pause(stream: Stream) {
        playlist[stream]?.pause(byUser = true)
    }

    override fun isPlayingAll(): Boolean {
        return playlist.values.any {
            !it.isPausedByUser && it.isPlaying
        }
    }

    override fun resumeAll() {
        playlist.values.forEach { it.resume(byUser = false) }
    }

    override fun pauseAll() {
        playlist.values.forEach { it.pause(byUser = false) }
    }

    override fun play(
        file: AudioFile,
        repeatCount: RepeatCount,
        frequency: Frequency,
        volume: Volume
    ): Stream {
        val id = count.incrementAndGet()
        val stream = Stream(id = id, fileId = id)
        val player = StreamPlayer(context).apply {
            play(file, repeatCount, frequency, volume)
        }
        playlist[stream] = player
        return stream
    }

    override fun stop(stream: Stream) {
        playlist.remove(stream)?.stop()
    }

    override fun updateFrequency(stream: Stream, frequency: Frequency) {
        playlist[stream]?.updateFrequency(frequency)
    }

    override fun updateVolume(stream: Stream, volume: Volume) {
        playlist[stream]?.updateVolume(volume)
    }
}

private class StreamPlayer(
    private val context: Context,
) {

    private val coroutineScope = CoroutineScope(Job() + Dispatchers.Main.immediate)

    private val mediaPlayerState = MutableStateFlow<MediaPlayer?>(null)

    @Volatile
    private var frequency: Frequency = Frequency.Default

    private val combinedOnCompletionListener = CombinedOnCompletionListener()

    var isPausedByUser = false

    val isPlaying: Boolean
        get() = mediaPlayerState.value?.isPlaying ?: false

    private var loopingJob: Job? = null

    val progress: Flow<Float>
        get() = mediaPlayerState.flatMapLatest {
            if (it == null) flowOf(0f)
            else flow {
                while (currentCoroutineContext().isActive) {
                    emit(it.currentPosition / it.duration.toFloat())
                    delay(10.milliseconds)
                }
            }.distinctUntilChanged()
        }

    fun play(
        file: AudioFile,
        repeatCount: RepeatCount,
        frequency: Frequency,
        volume: Volume
    ) {
        if (mediaPlayerState.value != null) {
            throw IllegalStateException("Already initialized!")
        }
        this.frequency = frequency
        if (repeatCount == RepeatCount.Forever) {
            startLooping()
        }
        mediaPlayerState.value = MediaPlayer().apply {
            setOnCompletionListener(combinedOnCompletionListener)
            setDataSource(file)
            setVolume(volume)
            isLooping = true
            prepare()
            start()
        }
    }

    fun stop() {
        cancelLooping()

        withPlayer { player ->
            player.setOnCompletionListener(null)
            player.release()
        }
        mediaPlayerState.value = null
    }

    fun pause(byUser: Boolean): Unit = withPlayer { player ->
        isPausedByUser = byUser
        player.pause()
        cancelLooping()
    }

    fun resume(byUser: Boolean): Unit = withPlayer { player ->
        if (byUser) {
            isPausedByUser = false
            player.start()
        } else {
            if (!isPausedByUser) {
                player.start()
            }
        }
        startLooping()
    }

    fun updateVolume(volume: Volume) = withPlayer { player ->
        player.setVolume(volume)
    }

    fun updateFrequency(frequency: Frequency) = withPlayer {
        this.frequency = frequency
        startLooping()
    }

    private fun cancelLooping() {
        loopingJob?.cancel()
        loopingJob = null
    }

    private fun startLooping() {
        cancelLooping()
        if (true) return
        loopingJob = coroutineScope.launch {
            while (isActive) {
                val player = mediaPlayerState.value ?: continue
                awaitCompletion()
                delay(frequency.delay)
                player.repeat()
            }
        }
    }

    private fun MediaPlayer.repeat() {
        seekTo(0)
        start()
    }

    private suspend fun awaitCompletion() = suspendCancellableCoroutine { continuation ->
        val listener = object : OnCompletionListener {
            override fun onCompletion(player: MediaPlayer) {
                combinedOnCompletionListener.removeListener(this)
                continuation.resume(Unit)
            }
        }
        combinedOnCompletionListener.addListener(listener)
        continuation.invokeOnCancellation {
            combinedOnCompletionListener.removeListener(listener)
        }
    }

    private inline fun withPlayer(action: (MediaPlayer) -> Unit) {
        val player = mediaPlayerState.value ?: throw IllegalStateException("Not initialized!")
        action.invoke(player)
    }

    private fun MediaPlayer.setDataSource(file: AudioFile) {
        when (file) {
            is AudioFile.Asset -> {
                val descriptor = context.assets.openFd(file.path)
                setDataSource(descriptor)
                descriptor.close()
            }

            is AudioFile.Path -> {
                setDataSource(context, Uri.fromFile(file.path))
            }
        }
    }

    private fun MediaPlayer.setVolume(volume: Volume) {
        setVolume(volume.value, volume.value)
    }
}

private class CombinedOnCompletionListener : OnCompletionListener {

    private val listeners = ArrayList<OnCompletionListener>()

    fun addListener(listener: OnCompletionListener) {
        listeners.add(listener)
    }

    fun removeListener(listener: OnCompletionListener) {
        listeners.remove(listener)
    }

    override fun onCompletion(mp: MediaPlayer?) {
        listeners.forEach { it.onCompletion(mp) }
    }
}