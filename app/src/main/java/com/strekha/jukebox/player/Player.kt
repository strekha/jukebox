@file:Suppress("FunctionName")

package com.strekha.jukebox.player

import kotlinx.coroutines.flow.Flow

interface Player {

    fun observeProgress(stream: Stream): Flow<Float>

    fun isPlaying(stream: Stream): Boolean

    fun resume(stream: Stream)

    fun pause(stream: Stream)

    fun isPlayingAll(): Boolean

    fun resumeAll()

    fun pauseAll()

    fun toggleAll() = if (isPlayingAll()) pauseAll() else resumeAll()

    fun play(
        file: AudioFile,
        repeatCount: RepeatCount = RepeatCount.Forever,
        frequency: Frequency = Frequency.Default,
        volume: Volume = Volume.Max,
    ): Stream

    fun stop(stream: Stream)

    fun updateFrequency(
        stream: Stream,
        frequency: Frequency
    )

    fun updateVolume(
        stream: Stream,
        volume: Volume
    )
}

@JvmInline
value class RepeatCount private constructor(val value: Int) {

    companion object {

        val Forever = RepeatCount(-1)

        val Once = RepeatCount(0)

        fun Fixed(value: Int): RepeatCount {
            require(value > 0)
            return RepeatCount(value)
        }
    }
}

data class Stream internal constructor(
    internal val id: Int,
    internal val fileId: Int,
)