package com.strekha.jukebox.presentation

import com.strekha.jukebox.player.AudioFile

data class Sample(
    val name: String,
    val file: AudioFile,
)