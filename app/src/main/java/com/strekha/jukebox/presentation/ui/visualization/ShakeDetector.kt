package com.strekha.jukebox.presentation.ui.visualization

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import kotlin.math.pow
import kotlin.math.sqrt


class ShakeDetector(
    context: Context,
    lifecycle: Lifecycle,
    private val onShake: () -> Unit,
) {

    private val service = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    private val accelerometer = service.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
    private var lastShakeTime: Long = 0

    private val listener = object : SensorEventListener {
        override fun onSensorChanged(event: SensorEvent?) {
            val curTime = System.currentTimeMillis()
            if (curTime - lastShakeTime > MIN_TIME_BETWEEN_SHAKES_MILLIS) {
                val x = event!!.values[0]
                val y = event.values[1]
                val z = event.values[2]
                val acceleration = sqrt(
                    x.toDouble().pow(2.0) +
                            y.toDouble().pow(2.0) +
                            z.toDouble().pow(2.0)
                ) - SensorManager.GRAVITY_EARTH
                if (acceleration > SHAKE_THRESHOLD) {
                    lastShakeTime = curTime
                    onShake.invoke()
                }
            }
        }

        override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        }
    }

    init {
        lifecycle.addObserver(object : DefaultLifecycleObserver {

            override fun onResume(owner: LifecycleOwner) {
                super.onResume(owner)
                service.registerListener(listener, accelerometer, SensorManager.SENSOR_DELAY_UI)
            }

            override fun onPause(owner: LifecycleOwner) {
                super.onPause(owner)
                service.unregisterListener(listener)
            }
        })
    }

    private companion object {

        private const val SHAKE_THRESHOLD = 3.25f // m/S**2
        private const val MIN_TIME_BETWEEN_SHAKES_MILLIS = 500
    }
}