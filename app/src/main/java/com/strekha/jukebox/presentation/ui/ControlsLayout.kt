package com.strekha.jukebox.presentation.ui

import androidx.annotation.DrawableRes
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.LocalContentColor
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.strekha.jukebox.R
import com.strekha.jukebox.presentation.JukeboxViewModel

@Composable
fun ControlsLayout(
    viewModel: JukeboxViewModel,
    onVisualizationClicked: () -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        modifier = modifier, horizontalArrangement = Arrangement.spacedBy(5.dp)
    ) {
        Box(modifier = Modifier.weight(1f)) {
            LayersButton(
                onClick = viewModel::onLayersButtonClick,
                isExpanded = viewModel.state.isLayersVisible
            )
        }
        IconButton(
            icon = R.drawable.ic_waves,
            onClick = remember(viewModel) { { onVisualizationClicked() } }, tint = LocalContentColor.current
        )
        IconButton(
            icon = R.drawable.ic_micro,
            onClick = viewModel::onMicrophoneButtonClick,
            tint = if (viewModel.state.isMicrophoneActive) Color.Red else LocalContentColor.current
        )
        IconButton(icon = R.drawable.ic_record, onClick = { /*TODO*/ })
        IconButton(
            icon = if (viewModel.state.isPlaying) R.drawable.ic_pause else R.drawable.ic_play,
            onClick = viewModel::onToggleOnClicked
        )
    }
}

@Composable
private fun Button(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    color: Color = MaterialTheme.colors.surface,
    contentPadding: PaddingValues = PaddingValues(all = 0.dp),
    content: @Composable RowScope.() -> Unit
) {
    CompositionLocalProvider(
        LocalContentColor provides Color(0xFF323232)
    ) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = modifier
                .sizeIn(minWidth = 34.dp, minHeight = 34.dp)
                .clip(RoundedCornerShape(4.dp))
                .background(color)
                .clickable { onClick() },
        ) {
            Row(
                modifier = Modifier.padding(contentPadding),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically,
                content = content
            )
        }

    }
}

@Composable
private fun LayersButton(
    onClick: () -> Unit,
    isExpanded: Boolean,
    modifier: Modifier = Modifier
) {
    val transition = updateTransition(isExpanded, label = "expanded")
    val arrowRotation = transition.animateFloat(label = "rotation") {
        if (it) 180f else 0f
    }
    val color = transition.animateColor(label = "color") {
        if (it) MaterialTheme.colors.primary else MaterialTheme.colors.surface
    }
    Button(
        onClick,
        modifier,
        color.value,
        contentPadding = PaddingValues(horizontal = 10.dp)
    ) {
        Text(
            text = "Слои",
            fontSize = 12.sp,
        )
        Spacer(modifier = Modifier.size(16.dp))
        Image(
            modifier = Modifier.rotate(arrowRotation.value),
            painter = painterResource(R.drawable.ic_arrow_up),
            contentDescription = null,
            colorFilter = ColorFilter.tint(LocalContentColor.current)
        )
    }
}

@Composable
private fun IconButton(
    @DrawableRes icon: Int,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    tint: Color = LocalContentColor.current,
) {
    Button(onClick, modifier) {
        Image(
            painter = painterResource(icon), contentDescription = null, colorFilter = ColorFilter.tint(tint)
        )
    }
}