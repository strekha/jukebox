package com.strekha.jukebox.presentation.ui

import android.widget.Toast
import androidx.activity.compose.BackHandler
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.expandVertically
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.strekha.jukebox.presentation.JukeboxViewModel

private const val InstrumentId = "Instrument"
private const val WorkingSurfaceId = "WorkingSurface"
private const val AudioWaveId = "AudioWave"
private const val ControlsLayoutId = "ControlsLayout"
private const val LayersLayoutId = "LayersLayout"

@Composable
fun JukeboxScreen(
    viewModel: JukeboxViewModel,
    navController: NavHostController
) {
    BackHandler(enabled = viewModel.state.isLayersVisible) {
        viewModel.onBackClick()
    }
    Layout(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background)
            .statusBarsPadding()
            .navigationBarsPadding()
            .padding(all = 15.dp),
        content = {
            viewModel.instruments.forEachIndexed { i, instrument ->
                Instrument(
                    instrument = instrument,
                    viewModel = viewModel,
                    modifier = Modifier.layoutId(InstrumentId + i)
                )
            }
            WorkingSurface(
                viewModel = viewModel,
                modifier = Modifier
                    .padding(top = 38.dp)
                    .fillMaxWidth()
                    .layoutId(WorkingSurfaceId)
            )
            val audioWaveAlpha by animateFloatAsState(if(viewModel.state.isLayersVisible) 0f else 1f)
            AudioWave(
                viewModel = viewModel,
                modifier = Modifier
                    .padding(top = 13.dp)
                    .fillMaxWidth()
                    .layoutId(AudioWaveId)
                    .graphicsLayer {
                        alpha = audioWaveAlpha
                    }
            )
            val context = LocalContext.current
            ControlsLayout(
                viewModel = viewModel,
                onVisualizationClicked = {
                    if (viewModel.state.layers.isNotEmpty()) {
                        navController.navigate("visualization")
                    } else {
                        Toast.makeText(context, "Сначала добавьте слои", Toast.LENGTH_SHORT).show()
                    }
                },
                modifier = Modifier
                    .padding(top = 10.dp)
                    .fillMaxWidth()
                    .layoutId(ControlsLayoutId)
            )
            AnimatedVisibility(
                visible = viewModel.state.isLayersVisible,
                enter = expandVertically(expandFrom = Alignment.Bottom) + fadeIn(),
                exit = shrinkVertically(shrinkTowards = Alignment.Bottom) + fadeOut(),
                modifier = Modifier
                    .layoutId(LayersLayoutId)
                    .padding(bottom = 10.dp)
            ) {
                LayersContent(
                    viewModel = viewModel,
                    modifier = Modifier.fillMaxSize()
                )
            }
        }
    ) { measurables, constraints ->


        val childConstraints = constraints.copy(minWidth = 0, minHeight = 0)

        // first measure all instrument layouts
        val instrumentPlaceables = measurables.filter { (it.layoutId as String).startsWith(InstrumentId) }
            .map { it.measure(childConstraints) }


        val audioWavePlaceable = measurables.first { it.layoutId == AudioWaveId }.measure(childConstraints)
        val controlsPlaceable = measurables.first { it.layoutId == ControlsLayoutId }.measure(childConstraints)

        val instrumentMinHeight = instrumentPlaceables.minOf { it.height }

        val spaceForWorkingSurface = constraints.maxHeight
            .minus(instrumentMinHeight)
            .minus(audioWavePlaceable.height)
            .minus(controlsPlaceable.height)
        val workingSurfaceConstraints = Constraints.fixed(constraints.maxWidth, spaceForWorkingSurface)
        val workingSurfacePlaceable = measurables.first { it.layoutId == WorkingSurfaceId }
            .measure(workingSurfaceConstraints)

        val layersPlaceable = measurables.firstOrNull{ it.layoutId == LayersLayoutId }?.let {
            val layersConstraints = Constraints.fixed(
                width = constraints.maxWidth,
                height = constraints.maxHeight - instrumentMinHeight - controlsPlaceable.height
            )
            it.measure(layersConstraints)
        }

        val width = constraints.maxWidth
        val height = constraints.maxHeight
        layout(width, height) {

            val instrumentsTotalWidth = instrumentPlaceables.sumOf { it.width }
            val spaceBetweenInstruments = (width - instrumentsTotalWidth) / instrumentPlaceables.lastIndex
            var instrumentOffsetX = 0
            instrumentPlaceables.forEach {
                it.place(instrumentOffsetX, 0, 1f)
                instrumentOffsetX += (it.width + spaceBetweenInstruments)
            }

            workingSurfacePlaceable.place(0, instrumentMinHeight)
            audioWavePlaceable.place(0, instrumentMinHeight + workingSurfacePlaceable.height)
            controlsPlaceable.place(0, instrumentMinHeight + workingSurfacePlaceable.height + audioWavePlaceable.height)
            layersPlaceable?.place(0, instrumentMinHeight)
        }
    }
}
