@file:Suppress("InfinitePropertiesLabel")

package com.strekha.jukebox.presentation.ui.visualization

import androidx.annotation.DrawableRes
import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.Easing
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.StartOffset
import androidx.compose.animation.core.VectorConverter
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.spring
import androidx.compose.animation.core.tween
import androidx.compose.foundation.Image
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.graphics.isSpecified
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import com.strekha.jukebox.R
import com.strekha.jukebox.presentation.JukeboxViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.random.Random
import kotlin.time.Duration
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

@Composable
fun VisualizationContainer(
    viewModel: JukeboxViewModel,
    modifier: Modifier = Modifier
) {

    val density = LocalDensity.current
    BoxWithConstraints(
        modifier = modifier
    ) {

        val containerHeight: Int = with(density) { maxHeight.roundToPx() }
        val containerWidth: Int = with(density) { maxWidth.roundToPx() }

        val figures = listOf(
            Figure(
                image = R.drawable.img_5,
                size = DpSize(230.dp, 400.dp),
                animations = setOf(
                    AnimationType.Gradient(from = Color(0xFF5A50E2), to = Color(0xFF101014)),
                    AnimationType.Floating(offset = with(density) { 5.dp.toPx() }),
                ),
                initialOffset = IntOffset(x = (containerWidth * 0.4f).toInt(), y = (containerHeight * 0.3).toInt()),
            ),
            Figure(
                image = R.drawable.img_9,
                size = DpSize(144.dp, 144.dp),
                animations = setOf(
                    AnimationType.Gradient(from = Color(0xFF5A50E2), to = Color(0xFFAAA6DA)),
                    AnimationType.Stretch(orientation = Orientation.Vertical),
                ),
                initialOffset = IntOffset(x = (containerWidth * 0.15f).toInt(), y = (containerHeight * 0.2).toInt()),
            ),
            Figure(
                image = R.drawable.img_1,
                size = DpSize(104.dp, 104.dp),
                animations = setOf(
                    AnimationType.Scale(min = 0.9f, max = 1.5f),
                    AnimationType.Spin(),
                ),
                initialOffset = IntOffset(x = (containerWidth * 0.6f).toInt(), y = (containerHeight * 0.36f).toInt())
            ),
            Figure(
                image = R.drawable.img_2,
                size = DpSize(216.dp, 230.dp),
                animations = setOf(
                    AnimationType.Spin(),
                    AnimationType.Stretch(orientation = Orientation.Horizontal)
                ),
                initialOffset = IntOffset(x = (containerWidth * 0.2f).toInt(), y = (containerHeight * 0.6).toInt())
            ),
            Figure(
                image = R.drawable.img_3,
                size = DpSize(93.dp, 100.dp),
                animations = setOf(
                    AnimationType.Scale(min = 0.9f, max = 1.5f, easing = LinearEasing),
                    AnimationType.Spin(duration = 1.seconds),
                ),
                initialOffset = IntOffset(x = (containerWidth * 0.1f).toInt(), y = (containerHeight * 0.3).toInt()),
                tint = Color(0xFF5A50E2),
            ),
            Figure(
                image = R.drawable.img_4,
                size = DpSize(70.dp, 90.dp),
                animations = setOf(
                    AnimationType.Gradient(from = Color(0xFFA8DB10), to = Color(0xFF47551F)),
                    AnimationType.Stretch(orientation = Orientation.Vertical, easing = LinearEasing)
                ),
                initialOffset = IntOffset(x = (containerWidth * 0.1f).toInt(), y = (containerHeight * 0.7).toInt()),
            ),
            Figure(
                image = R.drawable.img_6,
                size = DpSize(122.dp, 129.dp),
                animations = setOf(
                    AnimationType.Scale(min = 0.3f, max = 2f, easing = LinearEasing, duration = 5.seconds),
                    AnimationType.Spin(duration = 5.seconds),
                    AnimationType.Floating(offset = with(density) { 30.dp.toPx() }),
                ),
                initialOffset = IntOffset(x = (containerWidth * 0.5f).toInt(), y = (containerHeight * 0.6).toInt()),
                tint = Color(0xFF5A50E2),
            ),
            Figure(
                image = R.drawable.img_7,
                size = DpSize(88.dp, 88.dp),
                animations = setOf(
                    AnimationType.Scale(min = 0.7f, max = 1.5f, easing = LinearEasing, duration = 4.seconds),
                    AnimationType.Spin(duration = 5.seconds, clockwise = false),
                ),
                initialOffset = IntOffset(x = (containerWidth * 0.2f).toInt(), y = (containerHeight * 0.05).toInt()),
            ),
            Figure(
                image = R.drawable.img_8,
                size = DpSize(180.dp, 170.dp),
                animations = setOf(
                    AnimationType.Spin(duration = 5.seconds, clockwise = false),
                ),
                initialOffset = IntOffset(x = (containerWidth * 0.4f).toInt(), y = (containerHeight * 0.0).toInt()),
                tint = Color.White
            ),
        )

        val context = LocalContext.current
        val lifecycle = LocalLifecycleOwner.current.lifecycle
        val scope = rememberCoroutineScope()
        val shakeDetector = remember {
            ShakeDetector(context, lifecycle, onShake = {
                figures.forEach { figureToAnimate ->
                    figureToAnimate.updateOffset(scope, containerHeight, containerWidth)
                }
            })
        }


        figures.forEach {
            AnimatableFigure(
                figure = it,
                animate = viewModel.state.isPlaying
            )
        }
    }
}

@Composable
private fun AnimatableFigure(
    figure: Figure,
    modifier: Modifier = Modifier,
    animate: Boolean = true,
) {
    val transition = rememberInfiniteTransition(label = "")

    val stretch = figure.animations.findType<AnimationType.Stretch>()
    val scale by figure.animations.findType<AnimationType.Scale>()
        ?.takeIf { animate }
        ?.let {
            transition.animateFloat(
                initialValue = it.min,
                targetValue = it.max,
                animationSpec = infiniteRepeatable(
                    animation = tween(it.duration.inWholeMilliseconds.toInt(), easing = it.easing),
                    repeatMode = it.repeatMode,
                    initialStartOffset = StartOffset(
                        offsetMillis = Random.nextInt(from = 0, until = 1000),
                    )
                )
            )
        }
        ?: stretch
            ?.takeIf { animate }
            ?.let {
                transition.animateFloat(
                    initialValue = 1f,
                    targetValue = 1.5f,
                    animationSpec = infiniteRepeatable(
                        animation = tween(it.duration.inWholeMilliseconds.toInt(), easing = it.easing),
                        repeatMode = RepeatMode.Reverse,
                    )
                )
            }
        ?: remember { mutableFloatStateOf(1f) }


    val rotation by figure.animations.findType<AnimationType.Spin>()
        ?.takeIf { animate }
        ?.let {
            transition.animateFloat(
                initialValue = 0f,
                targetValue = 360f * if (it.clockwise) 1 else -1,
                animationSpec = infiniteRepeatable(
                    animation = tween(it.duration.inWholeMilliseconds.toInt(), easing = LinearEasing),
                    repeatMode = RepeatMode.Restart,
                )
            )
        }
        ?: remember { mutableFloatStateOf(0f) }

    val gradientAnimation = figure.animations.findType<AnimationType.Gradient>()
        ?.takeIf { animate }

    val gradientModifier = if (gradientAnimation != null) {
        val offset = transition.animateFloat(
            initialValue = 0f,
            targetValue = 1000f,
            animationSpec = infiniteRepeatable(
                animation = tween(gradientAnimation.duration.inWholeMilliseconds.toInt(), easing = LinearEasing),
                repeatMode = RepeatMode.Restart
            )
        )

        Modifier.drawWithContent {
            drawContent()
            drawRect(
                brush = Brush.horizontalGradient(
                    colors = listOf(gradientAnimation.from, gradientAnimation.to, gradientAnimation.from),
                    startX = 0f,
                    endX = offset.value
                ),
                blendMode = BlendMode.SrcAtop
            )
        }
    } else {
        Modifier
    }

    val offset = figure.animations.findType<AnimationType.Floating>()
        ?.takeIf { animate }
        ?.let {
            transition.animateFloat(
                initialValue = figure.offset(LocalDensity.current).y.toFloat(),
                targetValue = it.offset,
                animationSpec = infiniteRepeatable(
                    animation = tween(it.duration.inWholeMilliseconds.toInt()),
                    repeatMode = RepeatMode.Reverse,
                    initialStartOffset = StartOffset(
                        offsetMillis = Random.nextInt(from = 0, until = 1000),
                    )
                )
            )
        }

    Image(
        painter = painterResource(figure.image),
        contentDescription = null,
        colorFilter = figure.tint.takeIf { it.isSpecified }?.let { ColorFilter.tint(it) },
        modifier = modifier
            .size(figure.size)
            .offset(figure.offset)
            .graphicsLayer {
                if (stretch != null) {
                    when (stretch.orientation) {
                        Orientation.Vertical -> scaleY = scale
                        Orientation.Horizontal -> scaleX = scale
                    }
                } else {
                    scaleX = scale
                    scaleY = scale
                }

                rotationZ = rotation

                if (gradientAnimation != null) {
                    alpha = 0.9f
                }

                translationY = offset?.value ?: 0f
            }
            .then(gradientModifier)
    )
}

private inline fun <reified T : AnimationType> Set<AnimationType>.findType(): T? {
    return firstOrNull { it is T }?.let { it as T }
}


private sealed interface AnimationType {

    data class Scale(
        val min: Float,
        val max: Float,
        val duration: Duration = 500.milliseconds,
        val repeatMode: RepeatMode = RepeatMode.Reverse,
        val easing: Easing = FastOutSlowInEasing
    ) : AnimationType

    data class Spin(
        val duration: Duration = 3.seconds,
        val clockwise: Boolean = true,
    ) : AnimationType

    data class Gradient(
        val from: Color,
        val to: Color,
        val duration: Duration = 1.seconds,
        val orientation: Orientation = Orientation.Vertical
    ) : AnimationType

    data class Floating(
        val offset: Float,
        val duration: Duration = 1.seconds,
    ) : AnimationType

    data class Stretch(
        val orientation: Orientation = Orientation.Vertical,
        val duration: Duration = 1.seconds,
        val easing: Easing = FastOutSlowInEasing,
    ) : AnimationType
}

@Stable
private class Figure(
    @DrawableRes val image: Int,
    val size: DpSize,
    val animations: Set<AnimationType>,
    initialOffset: IntOffset,
    val tint: Color = Color.Unspecified
) {

    private var animateJob: Job? = null

    private val animatableOffset = Animatable(
        initialValue = initialOffset,
        typeConverter = IntOffset.VectorConverter,
    )

    @Stable
    val offset: Density.() -> IntOffset = { animatableOffset.value }

    fun updateOffset(
        scope: CoroutineScope,
        containerHeight: Int,
        containerWidth: Int,
    ) {
        animateJob?.cancel()
        val newOffset = IntOffset(
            x = Random.nextInt(from = 0, until = (containerWidth / 1.5f).toInt()),
            y = Random.nextInt(from = 0, until = (containerHeight / 1.5f).toInt()),
        )
        animateJob = scope.launch {
            animatableOffset.animateTo(
                targetValue = newOffset,
                animationSpec = spring(Spring.DampingRatioHighBouncy, Spring.StiffnessVeryLow)
            )
        }
    }
}