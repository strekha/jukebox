package com.strekha.jukebox.presentation

import androidx.compose.runtime.Stable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.strekha.jukebox.amplitudesParser
import com.strekha.jukebox.audioRecorder
import com.strekha.jukebox.instrumentsProvider
import com.strekha.jukebox.io.AmplitudesParser
import com.strekha.jukebox.io.AudioRecorder
import com.strekha.jukebox.io.InstrumentsProvider
import com.strekha.jukebox.player
import com.strekha.jukebox.player.AudioFile
import com.strekha.jukebox.player.Frequency
import com.strekha.jukebox.player.Player
import com.strekha.jukebox.player.RepeatCount
import com.strekha.jukebox.player.Stream
import com.strekha.jukebox.player.Volume
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlin.math.roundToInt
import kotlin.time.Duration.Companion.milliseconds

@Stable
class JukeboxViewModel(
    private val player: Player,
    private val amplitudesParser: AmplitudesParser,
    private val audioRecorder: AudioRecorder,
    instrumentsProvider: InstrumentsProvider
) : ViewModel() {

    val state = JukeboxState()

    private val _requestPermissionFlow = MutableSharedFlow<Unit>(extraBufferCapacity = 1)
    val requestPermission = _requestPermissionFlow.asSharedFlow()

    val instruments: List<Instrument> = instrumentsProvider.getInstruments()

    private var hoveredStream: Stream? = null
    private var updateAmplitudesJob: Job? = null

    private var observeProgressJob: Job? = null

    fun onMicrophoneButtonClick() {
        if (state.isMicrophoneActive) {
            state.isMicrophoneActive = false
            viewModelScope.launch {
                val outputFile = audioRecorder.stop()
                addLayer(
                    source = AudioFile.Path(outputFile),
                    name = generateLayerName(state.layers, prefix = "Запись")
                )
                player.resumeAll()
            }
        } else {
            _requestPermissionFlow.tryEmit(Unit)
        }
    }

    fun onGotMicrophonePermissionResult(isGranted: Boolean) {
        viewModelScope.launch {
            if (isGranted) {
                player.pauseAll()
                state.isMicrophoneActive = true
                audioRecorder.start()
            }
        }
    }

    fun onBackClick() {
        if (state.isLayersVisible) {
            state.isLayersVisible = false
        }
    }

    fun onLayersButtonClick() {
        when {
            state.isLayersVisible -> state.isLayersVisible = false
            state.layers.isNotEmpty() -> state.isLayersVisible = true
        }
    }

    fun onLayerClick(layer: Layer) {
        with(state) {
            isLayersVisible = false
            activeLayer = layer
        }
        updateAmplitudes(layer)
        observeLayerProgress(layer)
    }

    fun onToggleLayerStateClick(layer: Layer) {
        val isPlayingNow = layer.isPlaying
        if (!isPlayingNow) {
            player.pauseAll()
            player.resume(layer.stream)
        } else {
            player.resumeAll()
        }
        layer.isPlaying = !isPlayingNow
    }

    fun onToggleLayerMuteClick(layer: Layer) {

    }

    fun onRemoveLayerClick(layer: Layer) {
        player.stop(layer.stream)
        with(state) {
            if (layers.size == 1) {
                isLayersVisible = false
                activeLayer = null
                layers = emptyList()
                amplitudes = emptyList()
            } else {
                layers = layers.filter { it != layer }
                if (activeLayer == layer) {
                    activeLayer = layers.first()
                }
            }
        }
    }

    fun onToggleOnClicked() {
        player.toggleAll()
        state.isPlaying = player.isPlayingAll()
    }

    fun onVolumeChanged(rawDelta: Float) {
        state.activeLayer?.let { activeLayer ->
            val currentVolume = state.volume
            val newVolume = (currentVolume.value - rawDelta)
                .coerceIn(Volume.MinValue, Volume.MaxValue)
                .let(::Volume)
            activeLayer.volume = newVolume
            player.updateVolume(activeLayer.stream, activeLayer.volume)
        }
    }

    fun onFrequencyChanged(rawDelta: Float) {
        state.activeLayer?.let { activeLayer ->
            val currentFrequency = state.frequency
            val newFrequency = (currentFrequency.delay.inWholeMilliseconds + rawDelta)
                .coerceIn(
                    Frequency.MinValue.inWholeMilliseconds.toFloat(),
                    Frequency.MaxValue.inWholeMilliseconds.toFloat()
                )
                .roundToInt()
                .milliseconds
                .let(::Frequency)

            activeLayer.frequency = newFrequency
            player.updateFrequency(activeLayer.stream, activeLayer.frequency)
        }
    }

    fun onInstrumentClick(instrument: Instrument) {
        addLayer(instrument, instrument.samples.first())
    }

    fun onInstrumentDragStarted(instrument: Instrument) {
        if (!state.isLayersVisible) {
            state.expandedInstrument = instrument
            player.pauseAll()
        }
    }

    fun onSampleHovered(sample: Sample?) {
        val prevHoveredSample = state.hoveredSample
        state.hoveredSample = sample

        if (sample != null && sample != prevHoveredSample) {
            hoveredStream?.let { player.stop(it) }
            val stream = player.play(sample.file, RepeatCount.Once)
            hoveredStream = stream
        }
    }

    fun onInstrumentDragEnded() {
        hoveredStream?.let { player.stop(it) }
        hoveredStream = null

        val currentInstrument = state.expandedInstrument
        val currentSample = state.hoveredSample
        if (currentInstrument != null && currentSample != null) {
            addLayer(currentInstrument, currentSample)
        }

        state.hoveredSample = null
        state.expandedInstrument = null
        player.resumeAll()
    }

    private fun addLayer(
        instrument: Instrument,
        sample: Sample
    ) = addLayer(
        source = sample.file,
        name = generateLayerName(state.layers, prefix = instrument.name)
    )

    private fun addLayer(
        source: AudioFile,
        name: String
    ) {
        viewModelScope.launch {
            val currentLayers = state.layers
            val layerStream = player.play(source, RepeatCount.Forever)
            val layer = Layer(
                name = name,
                stream = layerStream,
                source = source
            )
            val newLayers = currentLayers + layer
            with(state) {
                layers = newLayers
                activeLayer = layer
                if (currentLayers.isEmpty()) {
                    isPlaying = true
                }
            }
            updateAmplitudes(layer)
            observeLayerProgress(layer)
        }
    }

    private fun updateAmplitudes(layer: Layer) {
        updateAmplitudesJob?.cancel()
        updateAmplitudesJob = viewModelScope.launch {
            state.amplitudes = amplitudesParser.getAmplitudeLevels(layer.source, sampleCount = 64)
        }
    }

    private fun observeLayerProgress(layer: Layer) {
        observeProgressJob?.cancel()
        observeProgressJob = viewModelScope.launch {observeProgressJob
            player.observeProgress(layer.stream).collectLatest {
                state.activeLayerProgress = it
            }
        }
    }

    /**
     * Returns next layer name in format "Instrument n".
     */
    private fun generateLayerName(
        currentLayers: List<Layer>,
        prefix: String
    ): String {
        fun generate(number: Int) = buildString {
            append(prefix)
            append(" ")
            append(number)
        }
        if (currentLayers.isEmpty()) {
            return generate(1)
        }
        val nextNumber = currentLayers.asSequence()
            .map { it.name }
            .filter { it.startsWith(prefix) }
            .map { it.removePrefix("$prefix ") }
            .map { it.toInt() }
            .maxOrNull()
            ?.plus(1)
            ?: 1
        return generate(nextNumber)
    }

    override fun onCleared() {
        observeProgressJob?.cancel()
        observeProgressJob = null

        updateAmplitudesJob?.cancel()
        updateAmplitudesJob = null
        super.onCleared()
    }
}

class JukeboxViewModelFactory : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return JukeboxViewModel(player, amplitudesParser, audioRecorder, instrumentsProvider) as T
    }
}