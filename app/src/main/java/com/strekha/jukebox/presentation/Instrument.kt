package com.strekha.jukebox.presentation

import androidx.annotation.DrawableRes

data class Instrument(
    val name: String,
    @DrawableRes val icon: Int,
    val samples: Set<Sample>
) {

    init {
        require(samples.isNotEmpty())
    }
}
