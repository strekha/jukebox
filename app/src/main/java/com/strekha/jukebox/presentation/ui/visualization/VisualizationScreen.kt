package com.strekha.jukebox.presentation.ui.visualization

import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.navigationBarsPadding
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.layout.statusBarsPadding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.strekha.jukebox.R
import com.strekha.jukebox.presentation.JukeboxViewModel

@Composable
fun VisualizationScreen(
    viewModel: JukeboxViewModel,
    navController: NavController,
) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colors.background)
            .statusBarsPadding()
            .navigationBarsPadding()
    ) {
        Toolbar(navController, viewModel, Modifier.fillMaxWidth())
        VisualizationContainer(
            viewModel = viewModel,
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
        )
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(48.dp),
            contentAlignment = Alignment.Center
        ) {
            val icon = if (viewModel.state.isPlaying) R.drawable.ic_pause else R.drawable.ic_play
            IconButton(onClick = viewModel::onToggleOnClicked) {
                Icon(
                    painter = painterResource(icon),
                    contentDescription = null,
                    tint = MaterialTheme.colors.primary
                )
            }
        }
    }
}

@Composable
private fun Toolbar(
    navController: NavController,
    viewModel: JukeboxViewModel,
    modifier: Modifier = Modifier
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .padding(horizontal = 15.dp)
            .fillMaxWidth()
    ) {
        ToolbarIcon(
            icon = R.drawable.ic_back,
            onClick = { navController.popBackStack() },
        )
        FileName(
            viewModel,
            modifier = Modifier
                .padding(horizontal = 16.dp)
                .weight(1f)
        )
        ToolbarIcon(
            icon = R.drawable.ic_download,
            tint = Color.Black,
            background = MaterialTheme.colors.primary,
            onClick = { },
        )
    }
}

@Composable
private fun ToolbarIcon(
    @DrawableRes icon: Int,
    modifier: Modifier = Modifier,
    background: Color = Color(0xFF5A50E2),
    tint: Color = Color.White,
    onClick: () -> Unit
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = modifier
            .requiredSize(34.dp)
            .clip(RoundedCornerShape(4.dp))
            .background(background)
            .clickable { onClick() },
    ) {
        Icon(
            painter = painterResource(icon),
            contentDescription = null,
            tint = tint,
        )
    }
}

@Composable
private fun FileName(
    viewModel: JukeboxViewModel,
    modifier: Modifier = Modifier,
) {
    TextField(
        modifier = modifier,
        value = viewModel.state.filename.value,
        onValueChange = { viewModel.state.filename.value = it },
        maxLines = 1,
        singleLine = true,
        colors = TextFieldDefaults.textFieldColors(
            textColor = Color.White,

            ),
        keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
        textStyle = LocalTextStyle.current.copy(
            color = Color.White,
            fontSize = 16.sp,
            fontWeight = FontWeight.Normal
        )
    )
}
