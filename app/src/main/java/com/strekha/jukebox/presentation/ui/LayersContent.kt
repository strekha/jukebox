package com.strekha.jukebox.presentation.ui

import androidx.compose.animation.animateColorAsState
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.strekha.jukebox.R
import com.strekha.jukebox.presentation.JukeboxViewModel
import com.strekha.jukebox.presentation.Layer

@Composable
fun LayersContent(
    viewModel: JukeboxViewModel,
    modifier: Modifier = Modifier
) {
    val activeLayer = viewModel.state.activeLayer
    LazyColumn(
        verticalArrangement = Arrangement.Bottom,
        modifier = modifier
    ) {
        items(
            viewModel.state.layers,
            key = { it.name }
        ) {
            Layer(
                viewModel = viewModel,
                layer = it,
                isActive = it.name == activeLayer?.name,
                modifier = Modifier.fillParentMaxWidth()
                    .padding(top = 10.dp)
                    .clickable { viewModel.onLayerClick(it) }
            )
        }
    }
}

@Composable
private fun Layer(
    viewModel: JukeboxViewModel,
    layer: Layer,
    isActive: Boolean,
    modifier: Modifier = Modifier
) {
    val color by animateColorAsState(
        targetValue = if (isActive) MaterialTheme.colors.primary else MaterialTheme.colors.surface,
        label = "color"
    )
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .height(40.dp)
            .clip(RoundedCornerShape(4.dp))
            .background(color),
    ) {
        Text(
            text = layer.name,
            fontSize = 12.sp,
            color = MaterialTheme.colors.onSurface,
            modifier = Modifier
                .padding(start = 10.dp)
                .weight(1f)
        )
        IconButton(
            onClick = { viewModel.onToggleLayerStateClick(layer) },
            modifier = Modifier.aspectRatio(1f)
        ) {
            val icon = if (layer.isPlaying) R.drawable.ic_pause else R.drawable.ic_play
            Icon(painterResource(icon), contentDescription = null)
        }
        IconButton(
            onClick = { viewModel.onToggleLayerMuteClick(layer) },
            modifier = Modifier.aspectRatio(1f)
        ) {
            val icon = if (layer.isMuted) R.drawable.ic_muted else R.drawable.ic_not_muted
            Icon(painterResource(icon), contentDescription = null)
        }
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .fillMaxHeight()
                .aspectRatio(1f)
                .clip(RoundedCornerShape(4.dp))
                .background(Color(0xFFE4E4E4))
                .clickable { viewModel.onRemoveLayerClick(layer) }
        ) {
            Image(painterResource(R.drawable.ic_remove), contentDescription = "Удалить")
        }
    }
}