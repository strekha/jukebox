@file:OptIn(ExperimentalFoundationApi::class, ExperimentalAnimationApi::class)

package com.strekha.jukebox.presentation.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.EnterExitState
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateColorAsState
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectDragGesturesAfterLongPress
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.hapticfeedback.HapticFeedback
import androidx.compose.ui.hapticfeedback.HapticFeedbackType
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.LayoutCoordinates
import androidx.compose.ui.layout.boundsInParent
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.layout.positionInParent
import androidx.compose.ui.platform.LocalHapticFeedback
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.strekha.jukebox.presentation.Instrument
import com.strekha.jukebox.presentation.JukeboxViewModel
import com.strekha.jukebox.presentation.Sample

@Composable
fun Instrument(
    instrument: Instrument,
    viewModel: JukeboxViewModel,
    modifier: Modifier = Modifier,
) {
    val state = viewModel.state
    val expandedInstrument = state.expandedInstrument
    val isExpanded = expandedInstrument == instrument
    val instrumentAlpha by animateFloatAsState(
        targetValue = if (expandedInstrument == null || isExpanded) 1f else 0.3f,
        label = "alpha"
    )
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier.graphicsLayer { alpha = instrumentAlpha },
    ) {

        val color by animateColorAsState(
            targetValue = if (isExpanded) MaterialTheme.colors.primary else MaterialTheme.colors.surface,
            label = "color"
        )
        val sampleLayoutCoordinates = remember { HashMap<Sample, LayoutCoordinates>(instrument.samples.size) }
        Column(
            modifier = Modifier
                .widthIn(max = 70.dp)
                .wrapContentHeight(unbounded = true)
                .clip(CircleShape)
                .background(color)
                .instrumentChooserDragHelper(
                    coordinatesBucket = sampleLayoutCoordinates,
                    instrument = instrument,
                    viewModel = viewModel,
                    haptics = LocalHapticFeedback.current
                )
        ) {
            Image(
                painter = painterResource(instrument.icon),
                colorFilter = ColorFilter.tint(MaterialTheme.colors.onSurface),
                contentDescription = null,
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f)
                    .clickable { viewModel.onInstrumentClick(instrument) }
            )
            AnimatedVisibility(isExpanded) {
                SampleChooser(
                    samples = instrument.samples,
                    selected = state.hoveredSample,
                    coordinatesBucket = sampleLayoutCoordinates,
                    isAnimationInProgress = { this.transition.currentState != EnterExitState.Visible },
                    modifier = Modifier.fillMaxWidth()
                )
            }
        }
        Text(
            modifier = Modifier.padding(top = 8.dp),
            text = instrument.name,
            fontSize = 12.sp,
            lineHeight = 14.sp,
            color = Color.White,
        )
    }
}

@Composable
private fun SampleChooser(
    samples: Set<Sample>,
    selected: Sample?,
    coordinatesBucket: MutableMap<Sample, LayoutCoordinates>,
    isAnimationInProgress: () -> Boolean,
    modifier: Modifier = Modifier,
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier.padding(vertical = 32.dp)
    ) {
        samples.forEach { sample ->
            Sample(
                sample = sample,
                isSelected = sample == selected,
                modifier = Modifier.onGloballyPositioned {
                    if (!isAnimationInProgress()) {
                        coordinatesBucket[sample] = it
                    }
                }
            )
        }
    }
}

@Composable
private fun Sample(
    sample: Sample,
    isSelected: Boolean,
    modifier: Modifier = Modifier,
) {
    val selectedBrush = Brush.verticalGradient(
        colors = listOf(Color.Transparent, Color.White, Color.White, Color.Transparent),
    )
    val backgroundModifier = if (isSelected) Modifier.background(selectedBrush) else Modifier
    Text(
        text = sample.name,
        color = Color.Black,
        fontSize = 12.sp,
        textAlign = TextAlign.Center,
        maxLines = 1,
        modifier = modifier
            .fillMaxWidth()
            .then(backgroundModifier)
            .padding(vertical = 4.dp)
            .graphicsLayer { this.alpha = alpha }
    )
}

private fun Modifier.instrumentChooserDragHelper(
    coordinatesBucket: MutableMap<Sample, LayoutCoordinates>,
    instrument: Instrument,
    viewModel: JukeboxViewModel,
    haptics: HapticFeedback,
) = pointerInput(Unit) {

    detectDragGesturesAfterLongPress(
        onDragStart = {
            haptics.performHapticFeedback(HapticFeedbackType.LongPress)
            viewModel.onInstrumentDragStarted(instrument)
        },
        onDragCancel = {
            viewModel.onInstrumentDragEnded()
            coordinatesBucket.clear()
        },
        onDragEnd = {
            viewModel.onInstrumentDragEnded()
            coordinatesBucket.clear()
        },
        onDrag = { change, _ ->
            val hoveredSample = coordinatesBucket.findSampleUnder(change.position)
            viewModel.onSampleHovered(hoveredSample)
        }
    )
}

private fun MutableMap<Sample, LayoutCoordinates>.findSampleUnder(
    position: Offset
): Sample? {
    return entries
        .firstOrNull {
            try {
                val coordinates = it.value
                val bounds = coordinates.boundsInParent()
                // since sample layout is nested, we need to offset position
                // offset relative to parent
                val offset = coordinates.parentLayoutCoordinates?.positionInParent() ?: Offset.Zero
                // ...and parent to root container, which actually handles touches
                val offset2 =
                    coordinates.parentLayoutCoordinates?.parentLayoutCoordinates?.positionInParent() ?: Offset.Zero
                bounds.contains(position - offset - offset2)
            } catch (t: IllegalStateException) {
                // some weird compose exception
                false
            }
        }
        ?.key
}
