package com.strekha.jukebox.presentation

import android.Manifest
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.strekha.jukebox.presentation.ui.JukeboxScreen
import com.strekha.jukebox.presentation.ui.visualization.VisualizationScreen
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private val viewModel by viewModels<JukeboxViewModel>(
        factoryProducer = { JukeboxViewModelFactory() }
    )

    private val recordAudio = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
        viewModel.onGotMicrophonePermissionResult(it)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        enableEdgeToEdge()
        super.onCreate(savedInstanceState)
        setContent {
            MaterialTheme(
                colors = darkColors(
                    primary = Color(0xFFA8DB10),
                    onPrimary = Color.Black,
                    background = Color.Black,
                    onBackground = Color.White,
                    surface = Color.White,
                    onSurface = Color.Black,
                )
            ) {
                Navigation()
            }
        }

        lifecycle.coroutineScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.CREATED) {
                viewModel.requestPermission.collect {
                    recordAudio.launch(Manifest.permission.RECORD_AUDIO)
                }
            }
        }
    }

    @Composable
    private fun Navigation() {
        val navController = rememberNavController()

        NavHost(navController = navController, startDestination = "jukebox") {
            composable("jukebox") { JukeboxScreen(viewModel, navController) }
            composable("visualization") { VisualizationScreen(viewModel, navController) }
        }
    }
}