package com.strekha.jukebox.presentation.ui

import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.drawWithContent
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.unit.dp
import com.strekha.jukebox.presentation.JukeboxViewModel
import kotlin.math.roundToInt

@Composable
fun AudioWave(
    viewModel: JukeboxViewModel,
    modifier: Modifier = Modifier
) {
    BoxWithConstraints(
        modifier = modifier.height(30.dp)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxSize()
        ) {
            val amplitudes = viewModel.state.amplitudes
            val maxAmplitude = amplitudes.maxOrNull()?.toFloat() ?: 0f

            val primaryColor = MaterialTheme.colors.primary
            val pastCount = (64 * viewModel.state.activeLayerProgress).roundToInt()
            repeat(64) { i ->
                val amplitude = amplitudes.getOrNull(i) ?: 0
                val size = when {
                    maxAmplitude == 0f || amplitude == 0 -> 2.dp
                    else -> this@BoxWithConstraints.maxHeight * amplitude / maxAmplitude
                }

                Box(
                    Modifier
                        .animateContentSize()
                        .requiredSize(width = 2.dp, height = size)
                        .clip(CircleShape)
                        .background(if (i <= pastCount) primaryColor else Color.White)
                )
            }
        }
    }
}