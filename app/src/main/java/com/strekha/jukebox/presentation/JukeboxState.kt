package com.strekha.jukebox.presentation

import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableFloatStateOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.strekha.jukebox.player.Frequency
import com.strekha.jukebox.player.Volume

@Stable
class JukeboxState {

    var expandedInstrument: Instrument? by mutableStateOf(null)

    var hoveredSample: Sample? by mutableStateOf(null)

    var activeLayer: Layer? by mutableStateOf(null)

    var activeLayerProgress: Float by mutableFloatStateOf(0f)

    var amplitudes: List<Int> by mutableStateOf(emptyList())

    var layers: List<Layer> by mutableStateOf(emptyList())

    var isLayersVisible by mutableStateOf(false)

    var isMicrophoneActive by mutableStateOf(false)

    var isPlaying: Boolean by mutableStateOf(false)

    @Stable
    val volume: Volume
        get() = activeLayer?.volume ?: Volume.Max

    @Stable
    val frequency: Frequency
        get() = activeLayer?.frequency ?: Frequency.Default

    @Stable
    var filename = mutableStateOf("Название трека")

}