package com.strekha.jukebox.presentation

import androidx.compose.runtime.Stable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import com.strekha.jukebox.player.AudioFile
import com.strekha.jukebox.player.Frequency
import com.strekha.jukebox.player.Stream
import com.strekha.jukebox.player.Volume

@Stable
class Layer(
    val name: String,
    val stream: Stream,
    val source: AudioFile,
) {

    var isPlaying: Boolean by mutableStateOf(false)

    var isMuted: Boolean by mutableStateOf(false)

    var volume: Volume by mutableStateOf(Volume.Max)

    var frequency: Frequency by mutableStateOf(Frequency.Default)
}