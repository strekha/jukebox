@file:OptIn(ExperimentalFoundationApi::class)

package com.strekha.jukebox.presentation.ui

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.DraggableState
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.gestures.draggable2D
import androidx.compose.foundation.gestures.rememberDraggable2DState
import androidx.compose.foundation.gestures.rememberDraggableState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.LocalTextStyle
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.Layout
import androidx.compose.ui.layout.layoutId
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.text.PlatformTextStyle
import androidx.compose.ui.unit.Constraints
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.strekha.jukebox.player.Frequency
import com.strekha.jukebox.player.Volume
import com.strekha.jukebox.presentation.JukeboxViewModel
import kotlin.math.roundToInt

private const val VolumeLevelCount = 31
private const val FrequencyLevelCount = 40

private val LevelBarSize = 14.dp
private val LevelBarShortSize = 7.dp
private val LevelBarWidth = 1.dp

@Composable
fun WorkingSurface(
    viewModel: JukeboxViewModel,
    modifier: Modifier = Modifier,
) {
    val state = viewModel.state
    val gradient = Brush.verticalGradient(
        colors = listOf(Color.Transparent, Color(0xFF5A50E2)),
    )

    var size by remember { mutableStateOf(IntSize.Zero) }

    fun onVolumeStateChanged(delta: Float) {
        val volumeDeltaFraction = delta / size.height
        val volumeDelta = volumeDeltaFraction * (Volume.MaxValue - Volume.MinValue)
        viewModel.onVolumeChanged(volumeDelta)
    }

    fun onFrequencyStateChanged(delta: Float) {
        val frequencyDeltaFraction = delta / size.width
        val frequencyDelta = frequencyDeltaFraction * (Frequency.MaxValue.inWholeMilliseconds - Frequency.MinValue.inWholeMilliseconds)
        viewModel.onFrequencyChanged(frequencyDelta)
    }

    val draggable2dState = rememberDraggable2DState { delta ->
        if (size == IntSize.Zero) return@rememberDraggable2DState

        onVolumeStateChanged(delta.y)
        onFrequencyStateChanged(delta.x)
    }

    val volumeDraggableState = rememberDraggableState { delta ->
        if (size == IntSize.Zero) return@rememberDraggableState
        onVolumeStateChanged(delta)
    }

    val frequencyDraggableState = rememberDraggableState { delta ->
        if (size == IntSize.Zero) return@rememberDraggableState
        onFrequencyStateChanged(delta)
    }

    Box(
        modifier = modifier
            .background(gradient)
            .draggable2D(draggable2dState)
            .onGloballyPositioned { size = it.size }
    ) {
        AnimatedVisibility(
            visible = !viewModel.state.isLayersVisible,
            enter = fadeIn(),
            exit = fadeOut()
        ) {
            Box {
                VolumeStatus(
                    currentVolume = state.volume,
                    draggableState = volumeDraggableState,
                    modifier = Modifier
                        .padding(bottom = 15.dp)
                        .fillMaxHeight()
                )
                FrequencyStatus(
                    currentFrequency = state.frequency,
                    draggableState = frequencyDraggableState,
                    modifier = Modifier
                        .padding(start = 15.dp)
                        .fillMaxWidth()
                        .align(Alignment.BottomCenter)
                )
            }
        }
    }
}

@Composable
private fun VolumeStatus(
    currentVolume: Volume,
    draggableState: DraggableState,
    modifier: Modifier = Modifier,
) {
    Layout(
        modifier = modifier,
        content = {
            VolumeBar(
                modifier = Modifier
                    .layoutId("bars")
                    .padding(bottom = 10.dp)
                    .fillMaxSize()
            )
            StatusLabel(
                text = "громкость",
                modifier = Modifier
                    .verticalLayout(clockwise = false)
                    .layoutId("label")
                    .draggable(draggableState, orientation = Orientation.Vertical)
            )
        },
    ) { measurables, constraints ->
        val labelConstraints = constraints.copy(minWidth = 0, minHeight = 0)
        val labelPlaceable = measurables
            .first { it.layoutId == "label" }
            .measure(labelConstraints)

        val barsConstraints = Constraints.fixed(
            width = LevelBarSize.roundToPx(),
            height = constraints.maxHeight
        )
        val barsPlaceable = measurables
            .first { it.layoutId == "bars" }
            .measure(barsConstraints)

        val layoutWidth = maxOf(labelPlaceable.width, barsPlaceable.width)
        val layoutHeight = constraints.maxHeight
        layout(layoutWidth, layoutHeight) {

            barsPlaceable.place(x = 0, y = 0, zIndex = 0f)

            // place label relative to current height
            // 0 - bottom, 1 - top
            val progress = (currentVolume.value - Volume.MinValue) / (Volume.MaxValue - Volume.MinValue)
            labelPlaceable.place(
                x = 0,
                y = ((layoutHeight - labelPlaceable.height) * (1 - progress)).roundToInt(),
                zIndex = 1f
            )
        }
    }
}

@Composable
private fun FrequencyStatus(
    currentFrequency: Frequency,
    draggableState: DraggableState,
    modifier: Modifier = Modifier,
) {
    Layout(
        modifier = modifier,
        content = {
            FrequencyBar(
                modifier = Modifier
                    .layoutId("bars")
                    .padding(start = 20.dp)
                    .fillMaxSize()
            )
            StatusLabel(
                text = "скорость",
                modifier = Modifier
                    .layoutId("label")
                    .draggable(draggableState, orientation = Orientation.Horizontal)
            )
        },
    ) { measurables, constraints ->
        val labelConstraints = constraints.copy(minWidth = 0, minHeight = 0)
        val labelPlaceable = measurables
            .first { it.layoutId == "label" }
            .measure(labelConstraints)

        val barsConstraints = Constraints.fixed(
            width = constraints.maxWidth,
            height = LevelBarSize.roundToPx()
        )
        val barsPlaceable = measurables
            .first { it.layoutId == "bars" }
            .measure(barsConstraints)

        val layoutWidth = constraints.maxWidth
        val layoutHeight = maxOf(labelPlaceable.height, barsPlaceable.height)
        layout(layoutWidth, layoutHeight) {

            barsPlaceable.place(
                x = 0,
                y = layoutHeight - barsPlaceable.height,
                zIndex = 0f
            )

            // place label relative to current height
            // 1 - left, 2 - right
            val progress = (currentFrequency.delay - Frequency.MinValue) / (Frequency.MaxValue - Frequency.MinValue)
            labelPlaceable.place(
                x = ((layoutWidth - labelPlaceable.width) * progress).roundToInt(),
                y = layoutHeight - labelPlaceable.height,
                zIndex = 1f
            )
        }
    }
}


@Composable
private fun StatusLabel(
    text: String,
    modifier: Modifier = Modifier
) {
    Text(
        text = text, color = MaterialTheme.colors.onPrimary,
        style = LocalTextStyle.current.copy(
            color = MaterialTheme.colors.onPrimary,
            fontSize = 11.sp,
            lineHeight = 13.sp,
            platformStyle = PlatformTextStyle(includeFontPadding = false)
        ),
        modifier = modifier
            .clip(RoundedCornerShape(4.dp))
            .background(MaterialTheme.colors.primary)
            .padding(horizontal = 6.dp, vertical = 1.dp)
    )
}

@Composable
private fun VolumeBar(modifier: Modifier = Modifier) {
    Canvas(
        modifier = modifier,
        onDraw = {
            val spaceBetweenBars = size.height / VolumeLevelCount
            repeat(VolumeLevelCount) { i ->
                val barSize = if (i % 5 == 0) LevelBarSize.toPx() else LevelBarShortSize.toPx()
                drawLine(
                    color = Color.White,
                    start = Offset(0f, spaceBetweenBars * i),
                    end = Offset(barSize, spaceBetweenBars * i),
                    strokeWidth = LevelBarWidth.toPx(),
                )
            }
        }
    )
}

@Composable
private fun FrequencyBar(modifier: Modifier = Modifier) {
    Canvas(
        modifier = modifier,
        onDraw = {
            val spaceBetweenBars = size.width / FrequencyLevelCount
            val yStart = size.height - LevelBarSize.toPx()
            val yEnd = size.height
            repeat(FrequencyLevelCount) { i ->
                val x = spaceBetweenBars * i
                drawLine(
                    color = Color.White,
                    start = Offset(x, yStart),
                    end = Offset(x, yEnd),
                    strokeWidth = LevelBarWidth.toPx(),
                )
            }
        }
    )
}