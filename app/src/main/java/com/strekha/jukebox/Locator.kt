package com.strekha.jukebox

import android.content.Context
import com.strekha.jukebox.io.AmplitudesParser
import com.strekha.jukebox.io.AudioRecorder
import com.strekha.jukebox.io.InstrumentsProvider
import com.strekha.jukebox.player.Player
import com.strekha.jukebox.player.mediaplayer.DefaultPlayer

val context: Context
    get() = JukeboxApplication.instance

val player: Player by lazy {
    DefaultPlayer(context)
}

val instrumentsProvider by lazy {
    InstrumentsProvider()
}

val amplitudesParser by lazy {
    AmplitudesParser(context)
}

val audioRecorder by lazy {
    AudioRecorder(context)
}