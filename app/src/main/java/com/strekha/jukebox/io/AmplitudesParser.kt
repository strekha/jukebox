package com.strekha.jukebox.io

import android.content.Context
import com.strekha.jukebox.player.AudioFile
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import linc.com.amplituda.Amplituda
import java.io.IOException
import java.io.InputStream
import kotlin.coroutines.suspendCoroutine
import kotlin.math.roundToInt

class AmplitudesParser(context: Context) {

    private val assets = context.assets

    private val amplitudeLib = Amplituda(context)

    suspend fun getAmplitudeLevels(
        source: AudioFile,
        sampleCount: Int
    ): List<Int> = withContext(Dispatchers.IO) {
        suspendCoroutine { continuation ->
            val inputStream = source.openInputStream()
            amplitudeLib.processAudio(inputStream)
                .get()
                .amplitudesAsList()
                .let { approximate(sampleCount, it) }
                .let { if (it.isEmpty()) Result.failure(IOException()) else Result.success(it) }
                .also {
                    try {
                        inputStream.close()
                    } catch (ignore: Throwable) {
                    }
                    continuation.resumeWith(it)
                }
        }
    }

    private fun AudioFile.openInputStream(): InputStream {
        return when (this) {
            is AudioFile.Asset -> assets.open(path)
            is AudioFile.Path -> path.inputStream()
        }
    }

    // linear approximation is used for now
    private fun approximate(
        requiredSampleCount: Int,
        amplitudes: List<Int>
    ): List<Int> {
        require(amplitudes.size > 1)

        if (amplitudes.size >= requiredSampleCount) {
            return approximate(requiredSampleCount, amplitudes.filterIndexed { index, _ -> index % 2 == 0 })
        }

        val result = ArrayList<Int>(requiredSampleCount).apply {
            repeat(requiredSampleCount) { add(-1) }
        }
        // map current positions to new positions
        val currentSize = amplitudes.size.toFloat()
        val mappingData = amplitudes.indices.map { i ->
            val percentInCurrentList = i / currentSize
            val indexInNewList = (percentInCurrentList * requiredSampleCount).roundToInt()
            indexInNewList to amplitudes[i]
        }

        for (i in 0..<mappingData.size - 1) {
            val (leftIndex, leftData) = mappingData[i]
            val (rightIndex, rightData) = mappingData[i + 1]

            result[leftIndex] = leftData
            result[rightIndex] = rightData

            // y = ax + b
            val a = (leftData - rightData) / (leftIndex - rightIndex)
            val b = -(a * leftIndex) + leftData

            for (subIndex in (leftIndex + 1)..<rightIndex) {
                result[subIndex] = a * subIndex + b
            }
        }
        return result
    }
}