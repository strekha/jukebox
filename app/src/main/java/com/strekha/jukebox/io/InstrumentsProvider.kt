package com.strekha.jukebox.io

import com.strekha.jukebox.R
import com.strekha.jukebox.player.AudioFile
import com.strekha.jukebox.presentation.Instrument
import com.strekha.jukebox.presentation.Sample

class InstrumentsProvider {

    fun getInstruments(): List<Instrument> = listOf(
        getGuitar(),
        getDrum(),
        getWinds(),
    )

    private fun getGuitar(): Instrument = Instrument(
        "Гитара",
        R.drawable.ic_guitar,
        setOf(
            Sample("семпл 1", AudioFile.Asset("guitar_1.wav")),
            Sample("семпл 2", AudioFile.Asset("guitar_2.wav")),
            Sample("семпл 3", AudioFile.Asset("guitar_3.wav")),
        )
    )

    private fun getDrum(): Instrument = Instrument(
        "Ударные",
        R.drawable.ic_drums,
        setOf(
            Sample("удар 1", AudioFile.Asset("kick_1.wav")),
            Sample("удар 2", AudioFile.Asset("kick_2.wav")),
            Sample("удар 3", AudioFile.Asset("kick_3.wav")),
        )
    )

    private fun getWinds(): Instrument = Instrument(
        "Духовые",
        R.drawable.ic_winds,
        setOf(
            Sample("звук 1", AudioFile.Asset("brass_1.wav")),
            Sample("звук 2", AudioFile.Asset("brass_2.wav")),
            Sample("звук 3", AudioFile.Asset("brass_3.wav")),
        )
    )
}