package com.strekha.jukebox.io

import android.content.Context
import android.media.MediaRecorder
import android.os.Build
import android.os.Environment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import kotlinx.coroutines.withContext
import java.io.File

class AudioRecorder(
    private val context: Context
) {

    private val mutex = Mutex()
    private var currentMediaRecorder: MediaRecorder? = null
    private var currentRecordingFile: File? = null

    // TODO: handle audio focus
    suspend fun start() = withContext(Dispatchers.IO) {
        mutex.withLock {
            if (currentRecordingFile != null || currentMediaRecorder != null) {
                throw IllegalStateException("Another recording is in progress!")
            }
            val outputFile = createOutputFile()

            currentRecordingFile = outputFile
            currentMediaRecorder = createMediaRecorder(outputFile).apply {
                start()
            }
        }
    }

    suspend fun stop(): File {
        mutex.withLock {
            currentMediaRecorder?.apply {
                stop()
                release()
            }
            val output = requireNotNull(currentRecordingFile)
            currentRecordingFile = null
            currentMediaRecorder = null
            return output
        }
    }

    private fun createOutputFile(): File {
        val directoryName = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            Environment.DIRECTORY_RECORDINGS
        } else {
            Environment.DIRECTORY_MUSIC
        }
        val outputDirectory = context.getExternalFilesDir(directoryName) ?: context.cacheDir
        return File.createTempFile("recording_", ".wav", outputDirectory)
    }

    private fun createMediaRecorder(output: File): MediaRecorder {
        val recorder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) {
            MediaRecorder(context)
        } else {
            MediaRecorder()
        }

        return recorder.apply {
            setAudioSource(MediaRecorder.AudioSource.MIC)
            setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                setOutputFile(output)
            } else {
                setOutputFile(output.absolutePath)
            }
            prepare()
        }
    }
}